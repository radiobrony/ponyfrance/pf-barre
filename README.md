# #LaBarre de Pony France

## Install/update en prod
- `git pull`
- Depuis le navigateur sur le domaine public accéder au `build.php`
- Intégrer le `pony-france.js` sur les sites, juste après l'ouverture du `<body>`

## Dev
- Lancer le serveur interne PHP dans le dossier du projet (`sudo php -S localhost:80`)
- Ouvrir `http://localhost/dev.php`
- Editer les fichiers et raffraîchir cette page
- Pour compiler le scss: `sass base.scss base.css --style compressed`
    - Pour installer sass: [`sudo gem install sass`](http://sass-lang.com/install)

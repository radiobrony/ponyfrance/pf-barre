<?php
require 'lib/CSSmin.php';
require 'lib/JSMin.php';
require 'lib/autoloader.php';

$url = '//' . $_SERVER['SERVER_NAME'] . dirname($_SERVER['REQUEST_URI']);
$cssmin = new CSSmin();
$css = addslashes($cssmin->run(str_replace('{url}', $url, file_get_contents('base.css'))));
$js = JSMin::minify(str_replace('{url}', $url, file_get_contents('base.js')));
$html = addslashes(str_replace(array("\n", "\r", "\t"), '', str_replace('{url}', $url, file_get_contents('base.html'))));
$final = str_replace('{css}', $css, str_replace('{html}', $html, $js));
file_put_contents('pony-france.js', $final);

header('Content-Type: text/javascript; charset=utf-8');
echo $final;

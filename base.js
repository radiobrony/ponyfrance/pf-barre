(function () {
	var div = document.createElement('div');
	div.innerHTML = '<style type="text/css">{css}</style>{html}';
	div.id = 'pony-france';
	document.body.appendChild(div);

	document.querySelector('#pony-france #pfLinkSelect').addEventListener('change', function () {
		location.href = this.value;
	});
}).call(this);

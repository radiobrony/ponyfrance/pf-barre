<?php
$url = '//' . $_SERVER['SERVER_NAME'] . dirname($_SERVER['REQUEST_URI']);
$css = str_replace('{url}', $url, file_get_contents('base.css'));
$html = str_replace('{url}', $url, file_get_contents('base.html'));
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Pony France</title>
	<style type="text/css">
<!-- {base.css} -->
<?php echo $css; ?>
<!-- {/base.css} -->
	</style>
</head>
<body>
<div id="pony-france">

<!-- {base.html} -->
<?php echo $html; ?>
<!-- {/base.html} -->

</div>

<!-- copy/paste useful js (based on base.js) -->
<script>
(function () {
        document.querySelector('#pony-france #pfLinkSelect').addEventListener('change', function () {
                location.href = this.value;
        });
}).call(this);
</script>
</body>
</html>
